use std::sync::{Arc, RwLock};
use std::thread;

// TYPES
type NodeRc = Arc<dyn Node + Sync + Send>;
type CounterRc = Arc<dyn Counter + Sync + Send>;

// STRUCTS

trait Counter {
    fn count(&self);
    fn report_count(&self);
}

struct CounterA {
    counter: RwLock<i32>
}

impl Counter for CounterA {
    fn count(&self) {
        let mut c = self.counter.write().unwrap();
        *c = *c + 1;
    }

    fn report_count(&self) {
        println!("CounterA: {}", self.counter.read().unwrap());
    }
}

struct CounterB {
    counter: RwLock<i32>
}

impl Counter for CounterB {
    fn count(&self) {
        let mut c = self.counter.write().unwrap();
        *c = *c + 2;
    }

    fn report_count(&self) {
        println!("CounterB: {}", self.counter.read().unwrap());
    }
}

trait Node {
    fn get_name(&self) -> &String;
    fn upgrade_name(&mut self);
    fn get_type(&self) -> String;
    fn get_children(&self) -> Option<&Vec<NodeRc>>;
    fn count(&self) {
        // do nothing by default
    }
    fn report_count(&self) {
        // do nothing by default
    }
}

struct NodeA {
    name: String,
    children: Vec<NodeRc>,
}

impl Node for NodeA {
    fn get_name(&self) -> &String {
        &self.name
    }

    fn upgrade_name(&mut self) {
        self.name = format!("{}1", self.name);
    }

    fn get_type(&self) -> String {
        "NodeA".to_string()
    }

    fn get_children(&self) -> Option<&Vec<NodeRc>> {
        Some(&self.children)
    }
}

struct NodeB {
    name: String,
    counter: CounterRc,
}

impl Node for NodeB {
    fn get_name(&self) -> &String {
        &self.name
    }

    fn upgrade_name(&mut self) {
        self.name = format!("{}2", self.name);
    }

    fn get_type(&self) -> String {
        "NodeB".to_string()
    }

    fn get_children(&self) -> Option<&Vec<NodeRc>> {
        None
    }

    fn count(&self) {
        self.counter.count();
    }

    fn report_count(&self) {
        self.counter.report_count();
    }
}

struct ThreadData {
    n: i32,
    root: NodeRc,
}

impl ThreadData {
    pub fn run(&self) {
        self.rec(&self.root);
    }

    fn rec(&self, node: &NodeRc) {
        println!(
            "THREAD: {}: {} - {}",
            self.n,
            node.get_type(),
            node.get_name()
        );
        node.count();
        if let Some(children) = node.get_children() {
            for child in children {
                self.rec(child);
            }
        }
        node.report_count();
    }
}

// MAIN

fn main() {
    // create data
    let children: Vec<NodeRc> = vec![
        Arc::new(NodeB{
            name: "/root/A".to_string(),
            counter: Arc::new(CounterA{counter: RwLock::new(0)})
        }),
        Arc::new(NodeB{
            name: "/root/B".to_string(),
            counter: Arc::new(CounterB{counter: RwLock::new(0)})
        }),
        Arc::new(NodeA{
            name: "/root/C".to_string(),
            children: Vec::new()
        })
    ];
    let root: NodeRc = Arc::new(
        NodeA{name: "/root".to_string(), children: children}
    );

    // spawn threads
    let mut handles = Vec::new();
    for i in 0..4 {
        let data = ThreadData{n: i, root: Arc::clone(&root)};
        handles.push(thread::spawn(move || {
            data.run();
        }));
    }

    for handle in handles {
        handle.join().unwrap();
    }
}
